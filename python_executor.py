import os
import subprocess
from subprocess import PIPE

COMMAND = 'COMMAND'
COMPILE = 'COMPILE'
RUN_TESTS = 'RUN_TESTS'

SOURCES_DIRECTORY = '/src'
DATA_DIRECTORY = '/src/test_data'

TASK_NAME = os.environ.get('TASK_NAME')
TASK_DIR = os.path.join(SOURCES_DIRECTORY, TASK_NAME)
TESTS_DIR = os.path.join(DATA_DIRECTORY, TASK_NAME)
MAIN_FILE = os.path.join(TASK_DIR, 'main.py')

if __name__ == '__main__':
    if os.environ.get('COMMAND') == COMPILE:
        print(os.environ.get('COMMAND'))
        raise Exception('Compiling for current language not supported.')
    if os.environ.get('COMMAND') != RUN_TESTS:
        print(os.environ.get('COMMAND'))
        raise Exception('Command not supported.')
    os.chdir(TASK_DIR)
    tests_dirs_names = sorted(os.listdir(TESTS_DIR), key=int)
    print('Start running tests.')
    for test in tests_dirs_names:
        print(test, end=' ')
        in_file = os.path.join(TESTS_DIR, test, test + '.in')
        process = subprocess.run('cat {} | python {}'.format(in_file, MAIN_FILE),
                                 shell=True, stdout=PIPE, stderr=PIPE)
        if process.returncode != 0:
            print('Error during execution program.')
            break
        with open(os.path.join(TESTS_DIR, test, test + '.out'), 'r') as out_file:
            if process.stdout.decode('utf-8') == out_file.read():
                print('OK')
            else:
                print('Output mismatch')
                break
    print('End running tests.')
