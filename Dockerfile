FROM python:3.6-alpine

WORKDIR /usr/src

COPY python_executor.py ./

ENTRYPOINT ["python", "./python_executor.py"]